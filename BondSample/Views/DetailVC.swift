//
//  DetailVC.swift
//  BondSample
//
//  Created by Slava Starovoitov on 01.12.2021.
//

import UIKit

class DetailVC: UIViewController {

    @IBOutlet private weak var bgImage: UIImageView!
    @IBOutlet private weak var posterImage: UIImageView!
    @IBOutlet private weak var songName: UILabel!
    @IBOutlet private weak var artistName: UILabel!
    @IBOutlet private weak var genre: UILabel!
    
    @IBOutlet weak var bufferView: UIView!
    
//    var viewModel: SongSearchViewModel?
    var viewModel = DetailResults.test()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        bufferView.isHidden = UIDevice.current.orientation.isLandscape
    }
    
    @IBAction func posterTapped(_ sender: Any) {
        let shareController = UIActivityViewController(activityItems: [posterImage.image as Any], applicationActivities: nil)
        present(shareController, animated: true, completion: nil)
    }
}

extension DetailVC {
    func setup() {
        bgImage.load(urlString: viewModel.images?.background ?? "")
        posterImage.load(urlString: viewModel.images?.coverart ?? "")
        posterImage.dropShadow(color: .clear, offSet: CGSize(width: 0, height: 0))
        songName.text = viewModel.title
        artistName.text = "by " + (viewModel.subtitle ?? "")
        genre.text = "Genre: " + (viewModel.genres?.primary ?? "")
        posterImage.isUserInteractionEnabled = true
    }
    
    
}
