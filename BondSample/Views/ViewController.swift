//
//  ViewController.swift
//  BondSample
//
//  Created by Slava Starovoitov on 21.11.2021.
//

import UIKit
import Bond
import ReactiveKit
import StoreKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var tableView: UITableView!
    
    public let viewModel = SongSearchViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.topItem?.title = "Back to list"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.title = "Search song"
    }
    
}

//MARK: - TableView Methods

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeaderView.identifier) as? CustomHeaderView else { return UIView() }
        
        headerView.tintColor = UIColor(named: "main")
        
        switch section {
            case 0:
                if tableView.numberOfRows(inSection: 0) > 0 {
                    headerView.setupText("Artists")
                } else {
                    headerView.setupText("")
                }
            case 1:
                if tableView.numberOfRows(inSection: 1) > 0 {
                    headerView.setupText("Songs")
                } else {
                    headerView.setupText("")
                }
            case 2:
                headerView.setupText("")
            default:
                break
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
//MARK: - Last indexPath
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex, !viewModel.endOfList {
            viewModel.loadMore()
        }
    }

}


//MARK: - ViewModel binding

extension ViewController {
    
    func bindViewModel() {
        
        viewModel.searchInProgress
            .map { !$0 }
            .bind(to: (searchBar.activityIndicator?.reactive.isHidden)!)
        
        viewModel.searchString.bidirectionalBind(to: searchBar.reactive.text)
        
        viewModel.validSearchText
            .map{ $0 ? .white : .systemPink }
            .bind(to: searchBar.reactive.tintColor)
        
        viewModel.data.bind(to: tableView) { dataSource, indexPath, tableView in
            
            let dataForm = dataSource[sectionAt: indexPath.section]
            
            let data = dataForm.items[indexPath.item]
            
            switch dataForm.metadata {
                case .artist:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CustomCell.identifier, for: indexPath) as? CustomCell else { return UITableViewCell() }
                    cell.setup(with: data, type: .artist)
                    return cell
                case .song:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CustomCell.identifier, for: indexPath) as? CustomCell else { return UITableViewCell() }
                    cell.setup(with: data, type: .song)
                    return cell
                case .loader:
                    guard let loadingCell = tableView.dequeueReusableCell(withIdentifier: LoadingCell.identifier, for: indexPath) as? LoadingCell else { return UITableViewCell() }
                    loadingCell.startAnimate(true)
                    return loadingCell
            }
        }
        
//MARK: - Error alert
        
        _ = viewModel.errorMessages.observeNext(with: { [unowned self] error in
            let alertController = UIAlertController(title: "Something went wrong", message: error, preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default,
                                         handler: { action in alertController.dismiss(animated: true, completion: nil) })
            
            alertController.addAction(actionOk)
            self.present(alertController, animated: true, completion: nil)
        })

        
//MARK: - Row selection
        
        _ = tableView.reactive.selectedRowIndexPath.observeNext(with: { [weak self] indexPath in
            
            let data = self?.viewModel.data[sectionAt: indexPath.section]
            
            switch data?.metadata {
                case .artist :
                    guard let artistName = data?.items[indexPath.item].name,
                          let url = URL(string: "spotify:search:" + artistName) else { return }
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        let vc = SKStoreProductViewController()
                        vc.loadProduct(withParameters:
                                        [SKStoreProductParameterITunesItemIdentifier : NSNumber(value: 324684580)],
                                       completionBlock: nil)
                        self?.present(vc, animated: true, completion: nil)
                    }
                case .song:
                    self?.view.addBlurAndSpinner()
                DispatchQueue.main.async {
                    guard let detailVC = self?.storyboard?.instantiateViewController(identifier: "DetailVC") as? DetailVC else { return }
                    self?.navigationController?.pushViewController(detailVC, animated: true)
                    self?.view.deleteBlurAndSpinner()
                }
                
                
//                    self?.viewModel.detailSearch(key: data?.items[indexPath.item].key ?? "", callback: {
//                        DispatchQueue.main.async {
//                            guard let detailVC = self?.storyboard?.instantiateViewController(identifier: "DetailVC") as? DetailVC else { return }
//                            detailVC.viewModel = self?.viewModel
//                            self?.navigationController?.pushViewController(detailVC, animated: true)
//                            self?.view.deleteBlurAndSpinner()
//                        }
//                    })
                    
                default :
                    break
            }
            
        }).dispose(in: bag)
    }
    
    func setup() {
        self.searchBar.isLoading = true
        self.navigationController?.custom(.main)
        self.tableView.register(CustomCell.nib, forCellReuseIdentifier: CustomCell.identifier)
        self.tableView.register(LoadingCell.nib, forCellReuseIdentifier: LoadingCell.identifier)
        self.tableView.register(CustomHeaderView.nib, forHeaderFooterViewReuseIdentifier: CustomHeaderView.identifier)
        bindViewModel()
        
    }
}




