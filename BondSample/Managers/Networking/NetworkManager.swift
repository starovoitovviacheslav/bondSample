//
//  NetworkManager.swift
//  BondSample
//
//  Created by Slava Starovoitov on 22.11.2021.
//

import Foundation

class NetworkManager {
    
    static let shared = NetworkManager()
    
    let headers = [
        "x-rapidapi-host": "shazam.p.rapidapi.com",
        "x-rapidapi-key": "594720a0abmsh6a543d494bc7b00p1f4f56jsn490282e8039b"
    ]
    
    func searchMusic(_ query: SongQuery, offset: Int, callback: @escaping (Result<SongsResult>) -> Void) {
        
        let urlString = "https://shazam.p.rapidapi.com/search?term=\(query.text)&locale=en-US&offset=\(String(offset))&limit=5"
        
        guard let url = URL(string: urlString) else { return }
        
        let request = NSMutableURLRequest(url: url)
        
        print("URL Requst: \(request)")
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        
        session.dataTask(with: request as URLRequest) { data, response, error in
            if let error = error {
                callback(.error(error))
            } else if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    let songs = try JSONDecoder().decode(SongsResult.self, from: data)
                    callback(.success(songs))
                } catch {
                    callback(.error(error))
                }
            }
        }.resume()
        
    }
    
    func searchDetail(_ key: String, callback: @escaping (Result<DetailResults>) -> Void) {
        
        let urlString = "https://shazam.p.rapidapi.com/songs/get-details?key=\(key)&locale=en-US"
        
        guard let url = URL(string: urlString) else { return }
        
        let request = NSMutableURLRequest(url: url)
        
        print("URL Requst: \(request)")
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        
        session.dataTask(with: request as URLRequest) { data, response, error in
            if let error = error {
                callback(.error(error))
            } else if let data = data {
                do {
                    let song = try JSONDecoder().decode(DetailResults.self, from: data)
                    callback(.success(song))
                    print("SongDetail: \(song)")
                } catch {
                    callback(.error(error))
                }
            }
        }.resume()
    }
}
