//
//  SceneDelegate.swift
//  BondSample
//
//  Created by Slava Starovoitov on 21.11.2021.
//

import UIKit
import Bond

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        print(URLContexts)
        for context in URLContexts {
            let urlComponents = URLComponents(url: context.url, resolvingAgainstBaseURL: true)
            let host = urlComponents?.host ?? ""
            print("host: \(host)")
            let sb = UIStoryboard(name: "Main", bundle: nil)
            
            let mainVC = sb.instantiateViewController(withIdentifier: "MainVC") as? ViewController
            self.window?.rootViewController = mainVC
            mainVC?.viewModel.searchString.value = host
        }
    }


}

