//
//  CustomHeaderView.swift
//  BondSample
//
//  Created by Slava Starovoitov on 23.11.2021.
//

import UIKit

extension UITableViewHeaderFooterView {
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}

class CustomHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet private weak var label: UILabel!
    
    func setupText(_ text: String) {
        label.text = text
    }
  
}
