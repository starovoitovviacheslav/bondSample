//
//  CustomCell.swift
//  BondSample
//
//  Created by Slava Starovoitov on 22.11.2021.
//

import UIKit

extension UITableViewCell {
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}

class CustomCell: UITableViewCell {
    
    @IBOutlet private weak var poster: UIImageView!
    @IBOutlet private weak var songName: UILabel!
    @IBOutlet private weak var artistName: UILabel!
    @IBOutlet private weak var bgView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bgView.layer.cornerRadius = 12
        self.poster.backgroundColor = .random
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.poster.image = nil
        
    }
    
    func setup(with data: SongItem, type: SongCellType) {
        
        artistName.text = data.song
        songName.text = data.name
        poster.load(urlString: data.poster)
        poster.dropShadow(color: .clear, offSet: CGSize(width: 0, height: 0))
        
        if type == .artist {
            artistName.isHidden = true
        } else {
            artistName.isHidden = false
        }
    }
    
}
