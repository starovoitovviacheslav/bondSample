//
//  LoadingCell.swift
//  BondSample
//
//  Created by Slava Starovoitov on 26.11.2021.
//

import UIKit

class LoadingCell: UITableViewCell {
    
    @IBOutlet private weak var actIndicator: UIActivityIndicatorView!
    
    func startAnimate(_ bool: Bool) {
        if bool {
            actIndicator.startAnimating()
        }
    }
    
}
