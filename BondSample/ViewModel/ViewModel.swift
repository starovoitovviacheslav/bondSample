//
//  ViewModel.swift
//  BondSample
//
//  Created by Slava Starovoitov on 21.11.2021.
//

import Foundation
import Bond
import ReactiveKit

class SongSearchViewModel {
    
    private let bag = DisposeBag()
    
    let searchString = Observable<String?>("")
    let validSearchText = Observable<Bool>(false)
    let searchInProgress = Observable<Bool>(false)
    let errorMessages = PassthroughSubject<String, Never>()
    
    private(set) var endOfList = false
    private var currentOffset = 5
    
//    let data = MutableObservableArray2D(Array2D<SongCellType, SongItem>(sectionsWithItems: [
//        (.artist, [SongItem]()),
//        (.song, [SongItem]()),
//        (.loader, [SongItem]())
//    ]))
    
    let data = MutableObservableArray2D(Array2D<SongCellType, SongItem>(sectionsWithItems: [
        (.artist, [SongItem(name: "test1", song: "test1", poster: "", key: "", type: .artist)]),
        (.song, [SongItem(name: "TEST2", song: "TEST2", poster: "", key: "", type: .artist)]),
        (.loader, [SongItem]())
    ]))
    
    var dataDetail = DetailResults.empty()
    
    func executeSearch(offset: Int) {
        
        searchInProgress.value = true
        
        var query = SongQuery()
        
        query.text = searchString.value?.replacingOccurrences(of: " ", with: "%20") ?? ""
        
        NetworkManager.shared.searchMusic(query, offset: offset) { [unowned self] result in
            
            switch result {
                case .success(let song):
                    if song.artists?.hits != nil || song.tracks?.hits != nil {
                        
                        let artistSection = song.artists?.hits
                        let trackSection = song.tracks?.hits
                        
                        let artistData = SongItem.makeArtistsArray(artistSection)
                        let songData = SongItem.makeSongArray(trackSection)
                        
                        data.batchUpdate { data in
                                
                            if offset == 0 {
                                data.replaceItems(ofSectionAt: 0, with: artistData)
                                data.replaceItems(ofSectionAt: 1, with: songData)
                                currentOffset = 5
                            } else {
                                artistSection?.forEach { artist in
                                    data.appendItem(SongItem.makeArtist(artist), toSectionAt: 0)
                                }
                                trackSection?.forEach { track in
                                    data.appendItem(SongItem.makeSong(track), toSectionAt: 1)
                                }
                            }
                            let loaderSection = [SongItem.emptyLoader()]
                            data.replaceItems(ofSectionAt: 2, with: loaderSection)
                        }
                        
                    } else {
                        data.removeSection(at: 2)
                        endOfList = true
                    }
                case .error (let error):
                    self.errorMessages.send(error.localizedDescription)
                }
            self.searchInProgress.value = false
        }
        
    }
    
    func loadMore() {
        executeSearch(offset: currentOffset)
        currentOffset += 5
    }
    
    func detailSearch(key: String, callback: @escaping () -> Void) {
        NetworkManager.shared.searchDetail(key) { [weak self] result in
            switch result {
                case .success(let song):
                    self?.dataDetail = song
                    callback()
                case .error(let error):
                    self?.errorMessages.send(error.localizedDescription)
            }
        }
    }
    
    init() {
        
        searchString
          .map { $0!.count > 3 }
          .bind(to:validSearchText)
        
        searchString
            .filter { $0!.count > 3 }
            .throttle(for: 2)
            .ignoreNils()
            .observeNext(with: { [weak self] text in
                self?.executeSearch(offset: 0)
            }).dispose(in: bag)
    }
}

