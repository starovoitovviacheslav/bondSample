//
//  NSCache + Pic.swift
//  BondSample
//
//  Created by Slava Starovoitov on 23.11.2021.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    
    func load(urlString: String) {
        
        if let image = imageCache.object(forKey: urlString as AnyObject) {
            self.image = image as? UIImage
            self.dropShadow(color: .black, opacity: 0.5, offSet: CGSize(width: 0, height: 5), radius: 15, scale: true)
            return
        } else {
            self.image = UIImage()
        }
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        imageCache.setObject(image, forKey: urlString as NSString)
                        self?.image = image
                        self?.dropShadow(color: .black, opacity: 0.5, offSet: CGSize(width: 0, height: 5), radius: 15, scale: true)
                    }
                } else {
                    self?.image = UIImage()
                }
            } else {
                DispatchQueue.main.async {
                    self?.image = UIImage()
                }
            }
        }
        
    }
}

