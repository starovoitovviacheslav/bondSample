//
//  UIView + Blur.swift
//  BondSample
//
//  Created by Slava Starovoitov on 02.12.2021.
//

import UIKit

extension UIView {
    
    func addBlurAndSpinner() {
        
        //blur
        let imageView = UIImageView(image: UIImage())
        imageView.frame = self.bounds
        imageView.contentMode = .scaleToFill
        self.addSubview(imageView)

        let blurEffect = UIBlurEffect(style: .systemUltraThinMaterialDark)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = imageView.bounds
        self.addSubview(blurredEffectView)
        
        let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
        let vibrancyEffectView = UIVisualEffectView(effect: vibrancyEffect)
        
        vibrancyEffectView.frame = imageView.bounds
        
        blurredEffectView.tag = 9
        
        //spinner
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.color = .white
        spinner.center = self.center
        spinner.startAnimating()
        self.addSubview(spinner)
        
        spinner.tag = 8
    }
    
    func deleteBlurAndSpinner() {
        DispatchQueue.main.async {
            self.viewWithTag(8)?.removeFromSuperview()
            self.viewWithTag(9)?.removeFromSuperview()
        }
    }
}
