//
//  Navigation + Colors.swift
//  BondSample
//
//  Created by Slava Starovoitov on 22.11.2021.
//

import UIKit

enum TypeVC {
    case main
    case detail
}

extension UINavigationController {
    func custom(_ type: TypeVC) {
        switch type {
            case .main:
                self.navigationBar.prefersLargeTitles = true
                self.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
            case .detail:
                self.navigationController?.navigationBar.prefersLargeTitles = false
                self.navigationController?.navigationBar.topItem?.title = "Back to list"
            }
    }
}
