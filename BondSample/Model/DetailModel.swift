//
//  DetailModel.swift
//  BondSample
//
//  Created by Slava Starovoitov on 01.12.2021.
//

import Foundation

struct DetailResults: Codable {
    let title: String?
    let subtitle: String?
    let images: DetailImages?
    let genres: Genres?
    
    static func empty() -> Self {
        return DetailResults(title: "", subtitle: "", images: nil, genres: nil)
    }
    
    static func test() -> Self {
        return DetailResults(title: "River Flows In You", subtitle: "Yiruma", images: DetailImages(background: "https://is4-ssl.mzstatic.com/image/thumb/Features115/v4/3f/84/4e/3f844e97-8c23-b64d-87f1-d536e99f80e9/mzl.rsxdpqqq.jpg/800x800cc.jpg", coverart: "https://is5-ssl.mzstatic.com/image/thumb/Music125/v4/b5/ae/26/b5ae2651-542a-9ee9-be4c-278791e2918f/mzi.ncrxyndi.jpg/400x400cc.jpg"), genres: Genres(primary: "Instrumental"))
    }
}

struct DetailImages: Codable {
    let background: String?
    let coverart: String?
}

struct Genres: Codable {
    let primary: String?
}
