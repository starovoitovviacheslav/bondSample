//
//  SongModel.swift
//  BondSample
//
//  Created by Slava Starovoitov on 22.11.2021.
//

import Foundation

enum SongCellType: Equatable {
    case artist
    case song
    case loader
}

struct SongsResult: Codable {
    let tracks: Tracks?
    let artists: Artists?
}

struct Artists: Codable {
    let hits: [ArtistHit]
}

struct Tracks: Codable {
    let hits: [TracksHit]
}

struct ArtistHit: Codable {
    let artist: HitArtist?
}

struct TracksHit: Codable {
    let track: Track?
}

struct HitArtist: Codable {
    let avatar: String?
    let name: String?
}

struct Track: Codable {
    let key: String?
    let title: String?
    let subtitle: String?
    let images: SongImages?
}

struct SongImages: Codable {
    let coverart: String?
}





