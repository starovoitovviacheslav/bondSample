//
//  SongProtocol.swift
//  BondSample
//
//  Created by Slava Starovoitov on 29.11.2021.
//

import Foundation


struct SongItem: Equatable {
    
    var name: String
    var song: String
    var poster: String
    var key: String
    
    var type: SongCellType
    
    static func emptyLoader() -> Self {
        return Self(name: "", song: "", poster: "", key: "", type: .loader)
    }
    
    static func makeArtist(_ artist: ArtistHit) -> Self {
        return Self(name: artist.artist?.name ?? "", song: artist.artist?.name ?? "", poster: artist.artist?.avatar ?? "", key: "", type: .artist)
    }
    
    static func makeSong(_ song: TracksHit) -> Self {
        return Self(name: song.track?.title ?? "", song: song.track?.subtitle ?? "", poster: song.track?.images?.coverart ?? "", key: song.track?.key ?? "", type: .song)
    }
    
    static func makeArtistsArray(_ artists: [ArtistHit]?) -> [Self] {
        var artistItemArray = [Self]()
        guard let artists = artists else { return artistItemArray }

        for artist in artists {
            let artistArray = Self.makeArtist(artist)
            artistItemArray.append(artistArray)
        }
        return artistItemArray
    }
    
    static func makeSongArray(_ songs: [TracksHit]?) -> [Self] {
        var songItemArray = [Self]()
        guard let songs = songs else { return songItemArray }
        
        for song in songs {
            let songArray = Self.makeSong(song)
            songItemArray.append(songArray)
        }
        return songItemArray
    }

}


