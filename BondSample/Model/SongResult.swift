//
//  SongResult.swift
//  BondSample
//
//  Created by Slava Starovoitov on 22.11.2021.
//

import Foundation

enum Result<T> {
  case success(T)
  case error(Error)
}
